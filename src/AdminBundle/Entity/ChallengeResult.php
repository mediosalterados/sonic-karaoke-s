<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="challenge_result")
 * @ORM\Entity
 *  
 */
class ChallengeResult {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \Entity\Currency
     *
     * @ORM\ManyToOne(targetEntity="Score", inversedBy="challenges")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="score_id", referencedColumnName="id")
     * })
     */
    private $score;

    /**
     * @var \Entity\Currency
     *
     * @ORM\ManyToOne(targetEntity="Challenge", inversedBy="results")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="challenge_id", referencedColumnName="id")
     * })
     */
    private $challenge;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param \AdminBundle\Entity\Score $score
     *
     * @return ChallengeResult
     */
    public function setScore(\AdminBundle\Entity\Score $score = null)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return \AdminBundle\Entity\Score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set challenge
     *
     * @param \AdminBundle\Entity\Challenge $challenge
     *
     * @return ChallengeResult
     */
    public function setChallenge(\AdminBundle\Entity\Challenge $challenge = null)
    {
        $this->challenge = $challenge;

        return $this;
    }

    /**
     * Get challenge
     *
     * @return \AdminBundle\Entity\Challenge
     */
    public function getChallenge()
    {
        return $this->challenge;
    }
}
