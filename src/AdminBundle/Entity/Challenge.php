<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="challenge")
 * @ORM\Entity
 *  
 */
class Challenge {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \Entity\Currency
     *
     * @ORM\ManyToOne(targetEntity="Score", inversedBy="challenges")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="score_id", referencedColumnName="id")
     * })
     */
    private $score;

    /** @ORM\Column(name="token", type="string") */
    protected $token;

    /** @ORM\Column(name="genero", type="string") */
    protected $genero;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ChallengeResult", mappedBy="challenge")
     */
    private $results;

    /**
     * Constructor
     */
    public function __construct() {
        $this->results = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set genero
     *
     * @param string $genero
     *
     * @return Challenge
     */
    public function setGenero($genero) {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string
     */
    public function getGenero() {
        return $this->genero;
    }

    /**
     * Set score
     *
     * @param \AdminBundle\Entity\Score $score
     *
     * @return Challenge
     */
    public function setScore(\AdminBundle\Entity\Score $score = null) {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return \AdminBundle\Entity\Score
     */
    public function getScore() {
        return $this->score;
    }

    /**
     * Add result
     *
     * @param \AdminBundle\Entity\ChallengeResult $result
     *
     * @return Challenge
     */
    public function addResult(\AdminBundle\Entity\ChallengeResult $result) {
        $this->results[] = $result;

        return $this;
    }

    /**
     * Remove result
     *
     * @param \AdminBundle\Entity\ChallengeResult $result
     */
    public function removeResult(\AdminBundle\Entity\ChallengeResult $result) {
        $this->results->removeElement($result);
    }

    /**
     * Get results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResults() {
        return $this->results;
    }


    /**
     * Set token
     *
     * @param string $token
     *
     * @return Challenge
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
