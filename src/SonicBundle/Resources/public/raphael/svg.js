
$(function()
{
   var MAX_R = 25,

       paper = Raphael(0, 0, $(window).width(), $(window).height()),
       $svg = $('svg'),

       wave = paper.circle(-1, -1, 1)
                    .attr('id', 'wave')
                    .attr('stroke', 'black')
                    .attr('fill-opacity', 0.4)
                    .attr('stroke-opacity', 0.3)
                    .hide(),

       charge = paper.circle(-1, -1, 1)
                      .attr('id', 'charge')
                      .attr('fill', 'red')
                      .attr('stroke', 'none')
                      .attr('fill-opacity', 0.4)
                      .hide(),

       pulse_x, pulse_y, intensity,
       timeline, pulse;

   timeline = new TimelineLite({
             paused: true
         });

/*
  Setup the timeline.  There are 4 distinct phases to
  out pulse animation:
  1 - MouseDown Initial charging - wave expands out to MAX_R
  2 - Mouse still down and fully charged, show pulsing red circle
  3 - Mouse up - collapse wave back to 0 radius
  4 - Release the wave expanding it until it animates off the screen

  Each part has a label so we can move the play head to that
  part of the animation.  Additionally, a functions are added to
  the timeline to setup, repeat, or complete various stages of the
  animation.
*/
   timeline.insertMultiple(
       [

      /*
         Part 1: Charge the wave.
          * Add the label for playback control
          * Add function to reset wave attributes and show
          * Add the animation
          * Add a function to show the pulsing circle
      */

          'charging',
          function ()
           {
              wave
                 .attr('fill', 'white')
                 .attr('fill-opacity', 0.4)
                 .attr('stroke-opacity', 0.3)
                 .show();
           },

          TweenLite.fromTo(wave, 0.5, {
                 raphael: {
                     r: 1,
                     'stroke-width': 1
                   }
             }, {
                 ease: Linear.easeNone,
                 raphael: {
                     r: MAX_R,
                     'stroke-width': 5
                   }
             }),

          function () {charge.show();},

      /*
         Part 2: Charged and waiting.
          * Add the label for playback control
          * Add the animation
          * Add a function to repeat the animation
      */

          'charged',
          TweenLite.fromTo(charge, 0.5, {
                 raphael: {
                     r: 1,
                     'fill-opacity': 0.8
                   }
             }, {
                 ease: Linear.easeNone,
                 raphael: {
                     r: MAX_R,
                     'fill-opacity': 0.05
                   }
             }),

          function () {timeline.play('charged')},

      /*
         Part 3: Start discharge process.
          * Add the label for playback control
          * Add function to hide the pulsing circle
          * Add the animation
      */

          'discharge',
          function () {charge.hide();},

          TweenLite.to(wave, 0.25, {
                 ease: Linear.easeNone,
                 raphael: {
                     r: 1,
                     'fill-opacity': 0,
                     'stroke-width': 1
                   }
             }),

      /*
         Part 4: Emit the wave.
          * Add the label for playback control
          * Add the animation - save a reference for later
          * Add function to hide everything
      */

          'pulse',
          (pulse = TweenLite.fromTo(wave, 0.15, {
                 raphael: {
                     r: 1,
                     'stroke-width': 1,
                     'stroke-opacity': 0.3
                   },
             }, {
                 ease: Linear.easeNone,
                 raphael: {
                     r: 1000,
                     'stroke-width': 100,
                     'stroke-opacity': 0.1
                   }
             })),

          function ()
           {
              charge.hide();
              wave.hide();

              pulse_x = -1;
              pulse_y = -1;
           }

       ], 0, 'sequence', 0.001);

   $svg
      .on('mousedown', function (e)
        {
           var pos = $(document.body).offset();

           pulse_x = e.pageX - pos.left;
           pulse_y = e.pageY - pos.top;

           wave
              .attr('cx', pulse_x)
              .attr('cy', pulse_y);

           charge
              .attr('cx', pulse_x)
              .attr('cy', pulse_y);

           timeline.play('charging');

        })
       .on('mouseup', function (e)
        {
           intensity = wave.attr('r') / MAX_R;

           /*
             Update target values based on run-time
             intensity calculation.  Need to use invalidate
             to clean out any caching and force recalculation
           */

           pulse.invalidate();
           pulse.vars.raphael['stroke-width'] = 100*intensity;
           pulse.vars.raphael['stroke-opacity'] = 0.3*intensity;

           timeline.play('discharge');
        });

});