/*Modified on 17 October*/
var audio = new Audio('/bundles/sonic/videos/' + genero + '/' + genero + '_mix1.mp3');
audio.volume = 0.5;
var timer = null;
var score = 0;
var browser = navigator.userAgent.toLowerCase();
var tmensaje = true;

ondas = setInterval(ondas, 2000);
flechas = setInterval(flechas, 7000);

var pause1 = null;
var trans = true;
var wow = true;
var sub = null;
var twow = 1;
var tbuu = 1;

var hp = 100;
var booMsg1Shown = false;
var booMsg2Shown = false;
var booMsg3Shown = false;
var booMsg4Shown = false;
var wowMsg1Shown = false;
var wowMsg2Shown = false;
var wowMsg3Shown = false;
var wowMsg4Shown = false;

function ondas()
{
    $("#ondas").fadeIn(1000);
    $("#ondas").fadeOut(1000);
}

function destello()
{

    $("#fade").show();
    $("#fade").animate({
        opacity: 0.0,
    }, 1500, function () {
        $("#fade").hide();
        $("#fade").css('opacity', '1');
    });

}

function flechas()
{
    $("#move_right").fadeIn(1000);
    $("#move_left").fadeIn(1000);
    $("#move_right").fadeOut(1000);
    $("#move_left").fadeOut(1000);
}

function fb_conect()
{
}

function get_tips()
{
}

function get_escenario()
{
}

function ocultar_evento()
{
    clearInterval(ondas);
    $('#ondas').hide(1000);
    $('#indicador').hide(1000);
}

function play()
{
    $("#bar_score").removeClass("disabled");
    $("#bar_iconBueno").removeClass("disabled");
    $("#bar_iconMalo").removeClass("disabled");
    $("#icon360").removeClass("disabled");
    audio.play();
    timer = setInterval(get_time, 100);
    activar_mensaje = setInterval(function () {
        tmensaje = false;
        mensajes(wow, 1);
    }, 10000);
    if (browser.indexOf('android') > -1)
    {
        player_feliz.play();
        player_triste.play();
    }
    else
    {
        //$('#chevrolet-triste').Valiant360('play');
        $('#chevrolet-feliz').Valiant360('play');
    }
    $('#play_btn').hide(1000);
    $('#pause').show(1000);
    ocultar_evento();
	
	hp = 60;
}


function continuar()
{
    pause1 = false;
    audio.play();
    if (browser.indexOf('android') > -1)
    {
        player_feliz.play();
        player_triste.play();
    }
    else
    {
        //$('#chevrolet-triste').Valiant360('play');
        $('#chevrolet-feliz').Valiant360('play');
    }
    //ZERO
    $('.overlay-wrap').addClass('hidden');
    $('#pause').show(1000);
    Fr.voice.resume();
}

function pause()
{
    pause1 = true;
    audio.pause();
    if (browser.indexOf('android') > -1)
    {
        player_feliz.pause();
        player_triste.pause();
    }
    else
    {
        //$('#chevrolet-triste').Valiant360('pause');
        $('#chevrolet-feliz').Valiant360('pause');
    }
    //ZERO
    $('.overlay-wrap').removeClass('hidden');
    $('#pause').hide(1000);
    Fr.voice.pause();

}

function get_time()
{
    time = audio.currentTime;
    document.getElementById("currentAudioTime").innerHTML = time;
    subtitulos(time);

    pichAudio = document.getElementById("pichAudio").innerHTML;
    get_score(pichAudio, time);

    if (pause1 !== true && hp > 0) {
        if (hp >= 50)
            hp -= 0.7;
        else
            hp -= 1.2;
    }

    switch (genero) {
        case "cumbia":
            if (time >= 104) {
                finish();
            }
        case "cumbia2":
        if (time >= 104) {
            finish();
        }
        case "pop":
            if (time >= 104) {
                finish();
            }
        case "rock":
            if (time >= 105) {
                finish();
            }
            break;
    }

    $("#line_bar_color").css("transform", "scale(1, " + hp / 100 + ")");
}

function finish()
{

    clearInterval(timer);
    clearInterval(flechas);
    //clearInterval(ova);
    //$("#container_score").show(500);
    // $('#indicador').show();

    $("#record").addClass("disabled");
    $('#continuar').hide(1000);
    $('#pause').hide(1000);

    if (browser.indexOf('android') > -1) {
        $("#chevrolet-feliz").remove();
    }
    else
    {
        $('#chevrolet-feliz').Valiant360('destroy');
        //$('#chevrolet-triste').Valiant360('destroy');
    }

    // Prevent user to win without doing anything.
    /*if(score <= 8000)
     score = 0;*/
    $('html').css('background-color', '000');
    $('*').fadeOut('slow');
    saveScore();
}

function saveScore() {
    $.ajax({
        url: url_to_score + '&puntaje=' + score,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.status === 'ok') {
                window.location.href = response.url;
            }
        }
    });
}

function save_record()
{
    Fr.voice.export(function (blob) {
        var formData = new FormData();
        formData.append('file', blob);

        /*
         $.ajax({
         url: "/server/upload_record.php",
         type: 'POST',
         data: formData,
         contentType: false,
         processData: false,
         success: function (url) {
         $.ajax({
         url: url_to_score + '&puntaje=' + score,
         type: 'POST',
         contentType: false,
         processData: false,
         success: function (response) {
         if (response.status === 'ok') {
         window.location.href = response.url;
         }
         }
         });
         }
         });*/
    }, "blob");
    Fr.voice.stop();
}



function reta_amigo()
{
}

function mensajes(wow, numero)
{
    if (wow === true)
    {
        if (tmensaje === false)
        {
            tmensaje = true;
            $("#wow > img").attr("src", "/bundles/sonic/img/wow" + numero + ".png");
            $("#wow").fadeIn(1000);
            $("#buu").fadeOut(500);
            setTimeout(function ()
            {
                $("#wow").fadeOut(500);
                setTimeout(function () {
                    tmensaje = false;
                }, 5000);
            }, 3000);
            clearInterval(activar_mensaje);
        }
    }
    else
    {
        if (tmensaje === false)
        {
            tmensaje = true;
            $("#buu > img").attr("src", "/bundles/sonic/img/buu" + numero + ".png");
            $("#buu").fadeIn(1000);
            $("#wow").fadeOut(500);
            setTimeout(function ()
            {
                $("#buu").fadeOut(500);
                setTimeout(function () {
                    tmensaje = false;
                }, 5000);
            }, 3000);
            clearInterval(activar_mensaje);
        }
    }
}

function transicion(wow)
{
    if (pause1 !== true)
    {

        if (trans != wow)
        {
            trans = wow;
            if (wow == true)
            {
                destello();


            }
            else
            {
                destello();


            }
        }
    }
}

function get_score(pichAudio, time)
{
    if (pause1 !== true)
    {
        /*if (pichAudio > 130 && pichAudio < 132)
         {
         score = score + 00;
         hp += 0.0;
         }
         else if (pichAudio > 133 && pichAudio < 135)
         {
         score = score + 40;
         hp += 9.0;
         }
         else if (pichAudio > 135 && pichAudio < 150)
         {
         score = score + 65;
         hp += 7.0;
         }*/
        if (pichAudio > 130 && pichAudio < 135)
        {
            score = score + 00;
            hp += 0.0;
        }
        else if (pichAudio > 135 && pichAudio < 138)
        {
            if (hp > 25)
                score = score + 40;

            if ((time >= 8.16 && time <= 13) || (time >= 17 && time <= 22.01) || (time >= 48.08 && time <= 52.05) || (time >= 52 && time <= 56.18) || (time >= 73.11 && time <= 78.08) || (time >= 73.11 && time <= 78.08) || (time >= 83 && time <= 97.03) || (time >= 87.3 && time <= 91.22) || time >= 43.24 && time <= 47.20)
                hp += 4.0;
            else
                hp += 8.0;
			
			if(browser.indexOf('android') > -1)
				hp += 2;
        }
        else if (pichAudio > 138 && pichAudio < 143)
        {
            if (hp > 25)
                score = score + 65;

            //if( (time >= 8.16 && time <= 13) || (time >= 17 && time <= 22.01) || (time >= 48.08 && time <= 52.05) || (time >= 52 && time <= 56.18) || (time >= 73.11 && time <= 78.08) || (time >= 73.11 && time <= 78.08) || (time >= 83 && time <= 97.03) || (time >= 87.3 && time <= 91.22) || time >= 43.24 && time <= 47.20)
            if ((time >= 8.16 && time <= 13) || (time >= 17 && time <= 22.01) || (time >= 48.08 && time <= 52.05) || (time >= 52 && time <= 56.18) || (time >= 73.11 && time <= 78.08) || (time >= 73.11 && time <= 78.08) || (time >= 83 && time <= 97.03) || (time >= 87.3 && time <= 91.22) || time >= 43.24 && time <= 47.20)
                hp += 20.0;
            else
                hp += 12.0;
			
			if(browser.indexOf('android') > -1)
				hp += 4;
        }
        else if (pichAudio > 143 && pichAudio < 150)
        {
            if (hp > 25)
                score = score + 65;

            if ((time >= 8.16 && time <= 13) || (time >= 17 && time <= 22.01) || (time >= 48.08 && time <= 52.05) || (time >= 52 && time <= 56.18) || (time >= 73.11 && time <= 78.08) || (time >= 73.11 && time <= 78.08) || (time >= 83 && time <= 97.03) || (time >= 87.3 && time <= 91.22) || time >= 43.24 && time <= 47.20)
                hp += 7.0;
            else
                hp += 15.0;

			if(browser.indexOf('android') > -1)
				hp += 6;
			

        }



        // Clamp hp value
        if (hp > 100)
            hp = 100;
    }

    /*if ((score >= 0 && time < 5) || (score > 500 && time == 10) || (score > 1000 && time == 20) || (score > 1500 && time == 30) || (score > 2000 && time == 40) || (score > 3000 && time == 50) || (score > 3500 && time == 60) || (score > 4000 && time == 70) || (score > 4500 && time == 80) || (score > 5000 && time == 90) || (score > 5500 && time == 90))
     {
     wow = true;
     transicion();
     }
     else
     {
     wow = false;
     transicion();
     }
     */
    /**/
    if (browser.indexOf('android') > -1)
    {
        move_bar = (hp * 2) - 200;
        /*$("#bar_triangulo").animate({bottom: ""+move_bar+"px"},100);
         $("#line_bar_color").animate({bottom: ""+move_bar+"px"},100);*/
    }
    else
    {
        move_bar = (hp * 3) - 300;
        /*$("#bar_triangulo").animate({bottom: ""+move_bar+"px"},100);*/
        /*$("#line_bar_color").animate({bottom: ""+move_bar+"px"},100);*/
        /*$("#line_bar_color").css("bottom", move_bar);*/

    }

    /*if(hp <= 12.5 && !booMsg1Shown){
     wow = false;
     //transicion(wow);
     booMsg1Shown = false;
     booMsg2Shown = false;
     booMsg3Shown = false;
     booMsg4Shown = false;
     wowMsg1Shown = false;
     wowMsg2Shown = false;
     wowMsg3Shown = false;
     wowMsg4Shown = false;
     booMsg1Shown = true;		
     mensajes(wow,1);
     }
     else if (hp > 12.5 && hp <=25 && !booMsg2Shown){
     wow = false;
     //transicion(wow);
     booMsg1Shown = false;
     booMsg2Shown = false;
     booMsg3Shown = false;
     booMsg4Shown = false;
     wowMsg1Shown = false;
     wowMsg2Shown = false;
     wowMsg3Shown = false;
     wowMsg4Shown = false;
     booMsg2Shown = true;
     mensajes(wow,2);
     }
     else if(hp > 25 && hp <= 37.5 && !booMsg3Shown){
     wow = false;
     //transicion(wow);
     booMsg1Shown = false;
     booMsg2Shown = false;
     booMsg3Shown = false;
     booMsg4Shown = false;
     wowMsg1Shown = false;
     wowMsg2Shown = false;
     wowMsg3Shown = false;
     wowMsg4Shown = false;
     booMsg3Shown = true;
     mensajes(wow,3);	
     }
     else if(hp > 37.5 && hp <= 50 && !booMsg4Shown){
     wow = false;
     //transicion(wow);
     booMsg1Shown = false;
     booMsg2Shown = false;
     booMsg3Shown = false;
     booMsg4Shown = false;
     wowMsg1Shown = false;
     wowMsg2Shown = false;
     wowMsg3Shown = false;
     wowMsg4Shown = false;
     booMsg4Shown = true;		
     mensajes(wow,4);
     }
     else if(hp > 50 && hp <= 62.5 && !wowMsg1Shown){
     wow = true;
     //transicion(wow);
     booMsg1Shown = false;
     booMsg2Shown = false;
     booMsg3Shown = false;
     booMsg4Shown = false;
     wowMsg1Shown = false;
     wowMsg2Shown = false;
     wowMsg3Shown = false;
     wowMsg4Shown = false;
     wowMsg1Shown = true;
     mensajes(wow,1);		
     }
     else if(hp > 62.5 && hp <= 75 && !wowMsg2Shown){
     wow = true;
     //transicion(wow);
     booMsg1Shown = false;
     booMsg2Shown = false;
     booMsg3Shown = false;
     booMsg4Shown = false;
     wowMsg1Shown = false;
     wowMsg2Shown = false;
     wowMsg3Shown = false;
     wowMsg4Shown = false;
     wowMsg2Shown = true; 
     mensajes(wow,2);	
     }
     else if(hp > 75 && hp <= 87.5 && !wowMsg3Shown){
     wow = true;
     //transicion(wow);
     booMsg1Shown = false;
     booMsg2Shown = false;
     booMsg3Shown = false;
     booMsg4Shown = false;
     wowMsg1Shown = false;
     wowMsg2Shown = false;
     wowMsg3Shown = false;
     wowMsg4Shown = false;
     wowMsg3Shown = true;
     mensajes(wow,3);	
     }
     else if(hp > 87.5 && !wowMsg4Shown){
     wow = true;
     //transicion(wow);
     booMsg1Shown = false;
     booMsg2Shown = false;
     booMsg3Shown = false;
     booMsg4Shown = false;
     wowMsg1Shown = false;
     wowMsg2Shown = false;
     wowMsg3Shown = false;
     wowMsg4Shown = false;
     wowMsg4Shown = true;	
     mensajes(wow,4);
     }*/

    if (hp <= 25 && !booMsg1Shown) {
        wow = false;
        booMsg1Shown = false;
        booMsg2Shown = false;
        wowMsg1Shown = false;
        wowMsg2Shown = false;
        booMsg1Shown = true;
        mensajes(wow, Math.floor((Math.random() * 4) + 1));
    }
    else if (hp > 25 && hp <= 50 && !booMsg2Shown) {
        wow = false;
        booMsg1Shown = false;
        booMsg2Shown = false;
        wowMsg1Shown = false;
        wowMsg2Shown = false;
        booMsg2Shown = true;
        mensajes(wow, Math.floor((Math.random() * 4) + 1));
    }
    /*else if(hp > 50 && hp <=75 && !wowMsg1Shown){
     wow = true;
     booMsg1Shown = false;
     booMsg2Shown = false;
     wowMsg1Shown = false;
     wowMsg2Shown = false;
     wowMsg1Shown = true;		
     mensajes(wow,Math.floor((Math.random() * 4) + 1));				
     }*/
    else if (hp > 75 && !wowMsg2Shown) {
        wow = true;
        booMsg1Shown = false;
        booMsg2Shown = false;
        wowMsg1Shown = false;
        wowMsg2Shown = false;
        wowMsg2Shown = true;
        mensajes(wow, Math.floor((Math.random() * 4) + 1));

    }


    // Transicion
    if (hp >= 50) {
        transicion(true);
        //mensajes(wow);
        if (browser.indexOf('android') > -1)
        {
            $("#chevrolet-feliz").fadeIn(750);
            $("#chevrolet-triste").fadeOut(750);
        }
        else
        {
            $('#chevrolet-feliz').Valiant360('DisplayVid1');
        }
    }
    else {
        transicion(false);
        //mensajes(wow);
        if (browser.indexOf('android') > -1)
        {
            $("#chevrolet-triste").fadeIn(750);
            $("#chevrolet-feliz").fadeOut(750);
        }
        else {
            $('#chevrolet-feliz').Valiant360('DisplayVid2');
        }
    }

    document.getElementById("value_score").innerHTML = score + ".00";
}

function get_ranking()
{

}

function subtitulos(time)
{
    
    switch (genero) {
        case "cumbia":
            executeCumbia();
            break;
        case "cumbia2":        
            executeCumbia2();
            break;
        case "pop":
            executePop();
            break;
        case "rock":
            executeRock();
            break;

    }
}

function executeRock() {

    if (time > 0 && time <= 4.02)
    {
        if (time > 0.14 && time <= 1.0)
            sub = "​<span>Ooh</span> All the girls stamp, your feet like this";
        if (time > 2.01 && time <= 3.0)
            sub = "​<span>Ooh All the girls</span> stamp,​ your feet like this";
        if (time > 3.0 && time <= 3.15)
            sub = "​<span>Ooh All the girls stamp,​ your feet</span> like this";
        if (time > 3.15 && time <= 4.02)
            sub = "​<span>Ooh All the girls stamp,​ your feet like this</span>";
    }
    else if (time >= 4.07 && time <= 8.13)
    {
        if (time >= 4.14 && time <= 5.04)
            sub = "​​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time > 5.04 && time <= 5.21)
            sub = "​<span>Few times​ I've been around</span> that track, So it's not just gonna happen like that";
        if (time > 5.21 && time <= 6.1)
            sub = "​<span>Few times I've been around that track</span>, So it's not just gonna happen like that";
        if (time > 6.1 && time <= 7.04)
            sub = "​<span>Few times I've been around that track, So it's not just</span> gonna happen like that";
        if (time > 7.04 && time <= 8.02)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen</span> like that";
        if (time > 8.02 && time <= 8.13)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 8.16 && time <= 13)
    {
        if (time >= 8.17 && time <= 9.0)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 9.07 && time <= 9.24)
            sub = "​<span>Cause I ain't no hollaback</span>​ girl I ain't no hollaback girl";
        if (time >= 9.24 && time <= 10.18)
            sub = "​<span>Cause I ain't no hollaback girl</span>​ I ain't no hollaback girl";
        if (time >= 10.22 && time <= 11.12)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no</span>​ hollaback girl";
        if (time >= 11.13 && time <= 12.09)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no​ hollaback</span> girl";
        if (time >= 12.09 && time <= 13)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no​ hollaback girl</span>";
    }
    else if (time >= 12.26 && time <= 17.09)
    {
        if (time >= 12.26 && time <= 14)
            sub = "​​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 13.2 && time <= 14.14)
            sub = "​<span>Few times​ I've been around</span> that track, So it's not just gonna happen like that";
        if (time >= 14.14 && time <= 14.23)
            sub = "​<span>Few times I've been around that track,​</span> So it's not just gonna happen like that";
        if (time >= 15.2 && time <= 16.09)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
        if (time >= 16.09 && time <= 17.09)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 17 && time <= 22.01)
    {
        if (time >= 16.27 && time <= 17.2)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 17.3 && time <= 18.22)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 18.22 && time <= 19.13)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 19.13 && time <= 21.01)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback</span> girl";
        if (time >= 21.01 && time <= 22.01)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 22.09 && time <= 25)
    {
        if (time >= 22.09 && time <= 23.15)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 23.15 && time <= 24.09)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 24.09 && time <= 25)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 25.2 && time <= 29.15)
    {
        if (time >= 25.2 && time <= 27.22)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 27.22 && time <= 28.1)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 28.1 && time <= 29.15)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 30.12 && time <= 35)
    {
        if (time >= 29.16 && time <= 31.1)
            sub = "​​<span>So that's right dude,</span> meet me at the bleachers No principals, no student-teachers";
        if (time >= 30.1 && time <= 32)
            sub = "​​<span>So that's right dude, meet me at the bleachers</span> No principals, no student-teachers";
        if (time >= 32 && time <= 33.16)
            sub = "​​<span>So that's right dude, meet me at the bleachers No principals,</span> no student-teachers";
        if (time >= 33.16 && time <= 35)
            sub = "​​<span>So that's right dude, meet me at the bleachers No principals, no student-teachers</span>";
    }
    else if (time >= 35 && time <= 38.9)
    {
        if (time >= 35 && time <= 35.20)
            sub = "<span>​​​Both of us wanna be</span> the winner, but there can only be one";
        if (time >= 36.09 && time <= 37)
            sub = "<span>​​​Both of us wanna be the winner,</span> but there can only be one";
        if (time >= 38 && time <= 38.9)
            sub = "​​​<span>Both of us wanna be the winner, but there can only be one</span>";
    }
    else if (time >= 39 && time <= 43.2)
    {
        if (time >= 39 && time <= 40)
            sub = "​<span>So I'm gonna fight,</span> gonna give it my all, Gonna make you fall, gonna sock it to ya";
        if (time >= 40 && time <= 41.06)
            sub = "​<span>So I'm gonna fight, gonna give it my all,</span> Gonna make you fall, gonna sock it to ya";
        if (time >= 41.06 && time <= 42.08)
            sub = "​<span>So I'm gonna fight, gonna give it my all, Gonna make you fall,</span> gonna sock it to ya";
        if (time >= 42.08 && time <= 43.2)
            sub = "​<span>So I'm gonna fight, gonna give it my all, Gonna make you fall, gonna sock it to ya</span>";
    }
    else if (time >= 43.24 && time <= 47.20)
    {
        if (time >= 43.24 && time <= 44.1)
            sub = "<span>​That's right,</span> I'm the last one standing, and another one bites the dust";
        if (time >= 44.1 && time <= 45.18)
            sub = "<span>​That's right, I'm the last one standing,</span> and another one bites the dust";
        if (time >= 45.18 && time <= 46.10)
            sub = "​<span>That's right, I'm the one standing, and another one</span> bites the dust";
        if (time >= 46.10 && time <= 47.20)
            sub = "​<span>That's right, I'm the one standing, and another one bites the dust</span>";
    }
    else if (time >= 48.08 && time <= 52.05)
    {
        if (time >= 48.06 && time <= 48.23)
            sub = "​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 48.23 && time <= 49.22)
            sub = "​<span>Few times I've been around that track,</span> So it's not just gonna happen like that";
        if (time >= 50.05 && time <= 51)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
        if (time >= 51 && time <= 52.04)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 52 && time <= 56.18)
    {
        if (time >= 52.11 && time <= 52.19)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 52.19 && time <= 53.16)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 53.16 && time <= 55.22)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 55.22 && time <= 56.18)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 57.15 && time <= 63.12)
    {
        if (time >= 57.15 && time <= 58.15)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 58.15 && time <= 59.09)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 62.01 && time <= 63.12)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 63.23 && time <= 69.15)
    {
        if (time >= 63.23 && time <= 65.07)
            sub = "​<span>Let me hear you say,</span> this s... is bananas, B-A-N-A-N-A-S";
        if (time >= 66.07 && time <= 67.04)
            sub = "​<span>Let me hear you say, this s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 67.04 && time <= 69.15)
            sub = "​<span>Let me hear you say, this s... is bananas, B-A-N-A-N-A-S</span>";
    }
    else if (time >= 70.16 && time <= 71.23)
    {
        if (time >= 70.1 && time <= 70.15)
            sub = "<span>is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 70.15 && time <= 71.23)
            sub = "<span>is bananas, B-A-N-A-N-A-S</span>";

    }
    else if (time >= 73.11 && time <= 78.08)
    {
        if (time >= 74.03 && time <= 74.16)
            sub = "<span>Again This s...</span> is bananas, B-A-N-A-N-A-S";
        if (time >= 74.16 && time <= 76.16)
            sub = "<span>Again This s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 76.16 && time <= 78.08)
            sub = "<span>Again This s... is bananas, B-A-N-A-N-A-S</span>";
    }
    else if (time >= 78.08 && time <= 82.17)
    {
        if (time >= 78.11 && time <= 79.03)
            sub = "<span>This s...</span> is bananas, B-A-N-A-N-A-S";
        if (time >= 79.10 && time <= 80.15)
            sub = "<span>This s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 80.19 && time <= 82.16)
            sub = "<span>This s... is bananas,B-A-N-A-N-A-S</span>";
    }
    else if (time >= 83 && time <= 87.02)
    {
        if (time >= 83 && time <= 83.22)
            sub = "​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 83.22 && time <= 86.03)
            sub = "​<span>Few times I've been around that track,</span> So it's not just gonna happen like that";
        if (time >= 86.03 && time <= 87.02)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
    }
    else if (time >= 87.3 && time <= 91.22)
    {
        if (time >= 87.3 && time <= 87.15)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 87.15 && time <= 88.15)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 88.15 && time <= 89.11)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 89.11 && time <= 90.17)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback</span> girl";
        if (time >= 90.17 && time <= 90.22)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 92.08 && time <= 95.08)
    {
        if (time >= 92.08 && time <= 93.09)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 93.09 && time <= 94.02)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 94.02 && time <= 95.04)
            sub = "<span>​Ooh This my s..., this my s...</span>";

    }
    else if (time >= 96.20 && time <= 97.17)
        sub = "<span>​Ooh </span> This my s..., this my s...";
    if (time >= 97.17 && time <= 98.09)
        sub = "<span>​Ooh This my s...,</span> this my s...";
    if (time >= 98.09 && time <= 99.16)
        sub = "<span>​Ooh This my s..., this my s...</span>";
    else if (time >= 99.16)
    {
        sub = "";
    }
    document.getElementById("sub_current").innerHTML = sub;
}

function executeCumbia() {

    if (time > 0 && time <= 4.02)
    {
        if (time > 0.14 && time <= 1.0)
            sub = "​<span>Ooh</span> All the girls stamp, your feet like this";
        if (time > 2.01 && time <= 3.0)
            sub = "​<span>Ooh All the girls</span> stamp,​ your feet like this";
        if (time > 3.0 && time <= 3.15)
            sub = "​<span>Ooh All the girls stamp,​ your feet</span> like this";
        if (time > 3.15 && time <= 4.02)
            sub = "​<span>Ooh All the girls stamp,​ your feet like this</span>";
    }
    else if (time >= 4.07 && time <= 8.13)
    {
        if (time >= 4.14 && time <= 5.04)
            sub = "​​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time > 5.04 && time <= 5.21)
            sub = "​<span>Few times​ I've been around</span> that track, So it's not just gonna happen like that";
        if (time > 5.21 && time <= 6.1)
            sub = "​<span>Few times I've been around that track</span>, So it's not just gonna happen like that";
        if (time > 6.1 && time <= 7.04)
            sub = "​<span>Few times I've been around that track, So it's not just</span> gonna happen like that";
        if (time > 7.04 && time <= 8.02)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen</span> like that";
        if (time > 8.02 && time <= 8.13)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 8.16 && time <= 13)
    {
        if (time >= 8.17 && time <= 9.0)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 9.07 && time <= 9.24)
            sub = "​<span>Cause I ain't no hollaback</span>​ girl I ain't no hollaback girl";
        if (time >= 9.24 && time <= 10.18)
            sub = "​<span>Cause I ain't no hollaback girl</span>​ I ain't no hollaback girl";
        if (time >= 10.22 && time <= 11.12)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no</span>​ hollaback girl";
        if (time >= 11.13 && time <= 12.09)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no​ hollaback</span> girl";
        if (time >= 12.09 && time <= 13)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no​ hollaback girl</span>";
    }
    else if (time >= 12.26 && time <= 17.09)
    {
        if (time >= 12.26 && time <= 14)
            sub = "​​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 13.2 && time <= 14.14)
            sub = "​<span>Few times​ I've been around</span> that track, So it's not just gonna happen like that";
        if (time >= 14.14 && time <= 14.23)
            sub = "​<span>Few times I've been around that track,​</span> So it's not just gonna happen like that";
        if (time >= 15.2 && time <= 16.09)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
        if (time >= 16.09 && time <= 17.09)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 17 && time <= 22.01)
    {
        if (time >= 16.27 && time <= 17.2)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 17.3 && time <= 18.22)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 18.22 && time <= 19.13)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 19.13 && time <= 21.01)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback</span> girl";
        if (time >= 21.01 && time <= 22.01)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 22.09 && time <= 25)
    {
        if (time >= 22.09 && time <= 23.15)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 23.15 && time <= 24.09)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 24.09 && time <= 25)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 25.2 && time <= 29.15)
    {
        if (time >= 25.2 && time <= 27.22)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 27.22 && time <= 28.1)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 28.1 && time <= 29.15)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 30.12 && time <= 35)
    {
        if (time >= 29.16 && time <= 31.1)
            sub = "​​<span>So that's right dude,</span> meet me at the bleachers No principals, no student-teachers";
        if (time >= 30.1 && time <= 32)
            sub = "​​<span>So that's right dude, meet me at the bleachers</span> No principals, no student-teachers";
        if (time >= 32 && time <= 33.16)
            sub = "​​<span>So that's right dude, meet me at the bleachers No principals,</span> no student-teachers";
        if (time >= 33.16 && time <= 35)
            sub = "​​<span>So that's right dude, meet me at the bleachers No principals, no student-teachers</span>";
    }
    else if (time >= 35 && time <= 38.9)
    {
        if (time >= 35 && time <= 35.20)
            sub = "<span>​​​Both of us wanna be</span> the winner, but there can only be one";
        if (time >= 36.09 && time <= 37)
            sub = "<span>​​​Both of us wanna be the winner,</span> but there can only be one";
        if (time >= 38 && time <= 38.9)
            sub = "​​​<span>Both of us wanna be the winner, but there can only be one</span>";
    }
    else if (time >= 39 && time <= 43.2)
    {
        if (time >= 39 && time <= 40)
            sub = "​<span>So I'm gonna fight,</span> gonna give it my all, Gonna make you fall, gonna sock it to ya";
        if (time >= 40 && time <= 41.06)
            sub = "​<span>So I'm gonna fight, gonna give it my all,</span> Gonna make you fall, gonna sock it to ya";
        if (time >= 41.06 && time <= 42.08)
            sub = "​<span>So I'm gonna fight, gonna give it my all, Gonna make you fall,</span> gonna sock it to ya";
        if (time >= 42.08 && time <= 43.2)
            sub = "​<span>So I'm gonna fight, gonna give it my all, Gonna make you fall, gonna sock it to ya</span>";
    }
    else if (time >= 43.24 && time <= 47.20)
    {
        if (time >= 43.24 && time <= 44.1)
            sub = "<span>​That's right,</span> I'm the last one standing, and another one bites the dust";
        if (time >= 44.1 && time <= 45.18)
            sub = "<span>​That's right, I'm the last one standing,</span> and another one bites the dust";
        if (time >= 45.18 && time <= 46.10)
            sub = "​<span>That's right, I'm the one standing, and another one</span> bites the dust";
        if (time >= 46.10 && time <= 47.20)
            sub = "​<span>That's right, I'm the one standing, and another one bites the dust</span>";
    }
    else if (time >= 48.08 && time <= 52.05)
    {
        if (time >= 48.06 && time <= 48.23)
            sub = "​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 48.23 && time <= 49.22)
            sub = "​<span>Few times I've been around that track,</span> So it's not just gonna happen like that";
        if (time >= 50.05 && time <= 51)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
        if (time >= 51 && time <= 52.04)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 52 && time <= 56.18)
    {
        if (time >= 52.11 && time <= 52.19)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 52.19 && time <= 53.16)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 53.16 && time <= 55.22)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 55.22 && time <= 56.18)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 57.15 && time <= 63.12)
    {
        if (time >= 57.15 && time <= 58.15)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 58.15 && time <= 59.09)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 62.01 && time <= 63.12)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 63.23 && time <= 69.15)
    {
        if (time >= 63.23 && time <= 65.07)
            sub = "​<span>Let me hear you say,</span> this s... is bananas, B-A-N-A-N-A-S";
        if (time >= 66.07 && time <= 67.04)
            sub = "​<span>Let me hear you say, this s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 67.04 && time <= 69.15)
            sub = "​<span>Let me hear you say, this s... is bananas, B-A-N-A-N-A-S</span>";
    }
    else if (time >= 70.16 && time <= 71.23)
    {
        if (time >= 70.1 && time <= 70.15)
            sub = "<span>is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 70.15 && time <= 71.23)
            sub = "<span>is bananas, B-A-N-A-N-A-S</span>";

    }
    else if (time >= 73.11 && time <= 78.08)
    {
        if (time >= 74.03 && time <= 74.16)
            sub = "<span>Again This s...</span> is bananas, B-A-N-A-N-A-S";
        if (time >= 74.16 && time <= 76.16)
            sub = "<span>Again This s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 76.16 && time <= 78.08)
            sub = "<span>Again This s... is bananas, B-A-N-A-N-A-S</span>";
    }
    else if (time >= 78.08 && time <= 82.17)
    {
        if (time >= 78.11 && time <= 79.03)
            sub = "<span>This s...</span> is bananas, B-A-N-A-N-A-S";
        if (time >= 79.10 && time <= 80.15)
            sub = "<span>This s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 80.19 && time <= 82.16)
            sub = "<span>This s... is bananas,B-A-N-A-N-A-S</span>";
    }
    else if (time >= 83 && time <= 87.02)
    {
        if (time >= 83 && time <= 83.22)
            sub = "​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 83.22 && time <= 86.03)
            sub = "​<span>Few times I've been around that track,</span> So it's not just gonna happen like that";
        if (time >= 86.03 && time <= 87.02)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
    }
    else if (time >= 87.3 && time <= 91.22)
    {
        if (time >= 87.3 && time <= 87.15)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 87.15 && time <= 88.15)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 88.15 && time <= 89.11)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 89.11 && time <= 90.17)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback</span> girl";
        if (time >= 90.17 && time <= 90.22)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 92.08 && time <= 95.08)
    {
        if (time >= 92.08 && time <= 93.09)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 93.09 && time <= 94.02)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 94.02 && time <= 95.04)
            sub = "<span>​Ooh This my s..., this my s...</span>";

    }
    else if (time >= 96.20 && time <= 97.17)
        sub = "<span>​Ooh </span> This my s..., this my s...";
    if (time >= 97.17 && time <= 98.09)
        sub = "<span>​Ooh This my s...,</span> this my s...";
    if (time >= 98.09 && time <= 99.16)
        sub = "<span>​Ooh This my s..., this my s...</span>";
    else if (time >= 99.16)
    {
        sub = "";
    }
    document.getElementById("sub_current").innerHTML = sub;
}


function executeCumbia2() {

    if (time > 0 && time <= 4.02)
    {   
        
        if (time > 0.14 && time <= 1.0)
            sub = "​<span>Ooh</span> This my s..., all the girls stamp, your feet like this";
        if (time > 1.03 && time <= 1.21)
            sub = "​<span>Ooh This my s...,</span> all the girls stamp,​ your feet like this";
        if (time > 2.05 && time <= 2.23)
            sub = "​<span>Ooh This my s..., all the girls</span> stamp,​ your feet like this";
        if (time > 2.23 && time <= 4.02)
            sub = "​<span>Ooh This my s..., all the girls stamp,​ your feet like this</span>";

    } 
    else if (time >= 4.07 && time <= 8.13)
    {
        if (time >= 4.14 && time <= 5.04)
            sub = "​​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time > 5.04 && time <= 5.21)
            sub = "​<span>Few times​ I've been around</span> that track, So it's not just gonna happen like that";
        if (time > 5.21 && time <= 6.1)
            sub = "​<span>Few times I've been around that track</span>, So it's not just gonna happen like that";
        if (time > 6.1 && time <= 7.04)
            sub = "​<span>Few times I've been around that track, So it's not just</span> gonna happen like that";
        if (time > 7.04 && time <= 8.02)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen</span> like that";
        if (time > 8.02 && time <= 8.13)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 8.16 && time <= 13)
    {
        if (time >= 8.17 && time <= 9.0)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 9.07 && time <= 9.24)
            sub = "​<span>Cause I ain't no hollaback</span>​ girl I ain't no hollaback girl";
        if (time >= 9.24 && time <= 10.18)
            sub = "​<span>Cause I ain't no hollaback girl</span>​ I ain't no hollaback girl";
        if (time >= 10.22 && time <= 11.12)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no</span>​ hollaback girl";
        if (time >= 11.13 && time <= 12.09)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no​ hollaback</span> girl";
        if (time >= 12.09 && time <= 13)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no​ hollaback girl</span>";
    }
    else if (time >= 12.26 && time <= 17.09)
    {
        if (time >= 12.26 && time <= 14)
            sub = "​​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 13.2 && time <= 14.14)
            sub = "​<span>Few times​ I've been around</span> that track, So it's not just gonna happen like that";
        if (time >= 14.14 && time <= 14.23)
            sub = "​<span>Few times I've been around that track,​</span> So it's not just gonna happen like that";
        if (time >= 15.2 && time <= 16.09)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
        if (time >= 16.09 && time <= 17.09)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 17 && time <= 22.01)
    {
        if (time >= 16.27 && time <= 17.2)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 17.3 && time <= 18.22)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 18.22 && time <= 19.13)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 19.13 && time <= 21.01)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback</span> girl";
        if (time >= 21.01 && time <= 22.01)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 22.09 && time <= 25)
    {
        if (time >= 22.09 && time <= 23.15)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 23.15 && time <= 24.09)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 24.09 && time <= 25)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 25.2 && time <= 29.15)
    {
        if (time >= 25.2 && time <= 27.22)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 27.22 && time <= 28.1)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 28.1 && time <= 29.15)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 30.12 && time <= 35)
    {
        if (time >= 29.16 && time <= 31.1)
            sub = "​​<span>So that's right dude,</span> meet me at the bleachers No principals, no student-teachers";
        if (time >= 30.1 && time <= 32)
            sub = "​​<span>So that's right dude, meet me at the bleachers</span> No principals, no student-teachers";
        if (time >= 32 && time <= 33.16)
            sub = "​​<span>So that's right dude, meet me at the bleachers No principals,</span> no student-teachers";
        if (time >= 33.16 && time <= 35)
            sub = "​​<span>So that's right dude, meet me at the bleachers No principals, no student-teachers</span>";
    }
    else if (time >= 35 && time <= 38.9)
    {
        if (time >= 35 && time <= 35.20)
            sub = "<span>​​​Both of us wanna be</span> the winner, but there can only be one";
        if (time >= 36.09 && time <= 37)
            sub = "<span>​​​Both of us wanna be the winner,</span> but there can only be one";
        if (time >= 38 && time <= 38.9)
            sub = "​​​<span>Both of us wanna be the winner, but there can only be one</span>";
    }
    else if (time >= 39 && time <= 43.2)
    {
        if (time >= 39 && time <= 40)
            sub = "​<span>So I'm gonna fight,</span> gonna give it my all, Gonna make you fall, gonna sock it to ya";
        if (time >= 40 && time <= 41.06)
            sub = "​<span>So I'm gonna fight, gonna give it my all,</span> Gonna make you fall, gonna sock it to ya";
        if (time >= 41.06 && time <= 42.08)
            sub = "​<span>So I'm gonna fight, gonna give it my all, Gonna make you fall,</span> gonna sock it to ya";
        if (time >= 42.08 && time <= 43.2)
            sub = "​<span>So I'm gonna fight, gonna give it my all, Gonna make you fall, gonna sock it to ya</span>";
    }
    else if (time >= 43.24 && time <= 47.20)
    {
        if (time >= 43.24 && time <= 44.1)
            sub = "<span>​That's right,</span> I'm the last one standing, and another one bites the dust";
        if (time >= 44.1 && time <= 45.18)
            sub = "<span>​That's right, I'm the last one standing,</span> and another one bites the dust";
        if (time >= 45.18 && time <= 46.10)
            sub = "​<span>That's right, I'm the one standing, and another one</span> bites the dust";
        if (time >= 46.10 && time <= 47.20)
            sub = "​<span>That's right, I'm the one standing, and another one bites the dust</span>";
    }
    else if (time >= 48.08 && time <= 52.05)
    {
        if (time >= 48.06 && time <= 48.23)
            sub = "​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 48.23 && time <= 49.22)
            sub = "​<span>Few times I've been around that track,</span> So it's not just gonna happen like that";
        if (time >= 50.05 && time <= 51)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
        if (time >= 51 && time <= 52.04)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 52 && time <= 56.18)
    {
        if (time >= 52.11 && time <= 52.19)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 52.19 && time <= 53.16)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 53.16 && time <= 55.22)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 55.22 && time <= 56.18)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 57.15 && time <= 63.12)
    {
        if (time >= 57.15 && time <= 58.15)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 58.15 && time <= 59.09)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 62.01 && time <= 63.12)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 63.23 && time <= 69.15)
    {
        if (time >= 63.23 && time <= 65.07)
            sub = "​<span>Let me hear you say,</span> this s... is bananas, B-A-N-A-N-A-S";
        if (time >= 66.07 && time <= 67.04)
            sub = "​<span>Let me hear you say, this s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 67.04 && time <= 69.15)
            sub = "​<span>Let me hear you say, this s... is bananas, B-A-N-A-N-A-S</span>";
    }
    else if (time >= 70.16 && time <= 71.23)
    {
        if (time >= 70.1 && time <= 70.15)
            sub = "<span>is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 70.15 && time <= 71.23)
            sub = "<span>is bananas, B-A-N-A-N-A-S</span>";

    }
    else if (time >= 73.11 && time <= 78.08)
    {
        if (time >= 74.03 && time <= 74.16)
            sub = "<span>Again This s...</span> is bananas, B-A-N-A-N-A-S";
        if (time >= 74.16 && time <= 76.16)
            sub = "<span>Again This s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 76.16 && time <= 78.08)
            sub = "<span>Again This s... is bananas, B-A-N-A-N-A-S</span>";
    }
    else if (time >= 78.08 && time <= 82.17)
    {
        if (time >= 78.11 && time <= 79.03)
            sub = "<span>This s...</span> is bananas, B-A-N-A-N-A-S";
        if (time >= 79.10 && time <= 80.15)
            sub = "<span>This s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 80.19 && time <= 82.16)
            sub = "<span>This s... is bananas,B-A-N-A-N-A-S</span>";
    }
    else if (time >= 83 && time <= 87.02)
    {
        if (time >= 83 && time <= 83.22)
            sub = "​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 83.22 && time <= 86.03)
            sub = "​<span>Few times I've been around that track,</span> So it's not just gonna happen like that";
        if (time >= 86.03 && time <= 87.02)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
    }
    else if (time >= 87.3 && time <= 91.22)
    {
        if (time >= 87.3 && time <= 87.15)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 87.15 && time <= 88.15)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 88.15 && time <= 89.11)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 89.11 && time <= 90.17)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback</span> girl";
        if (time >= 90.17 && time <= 90.22)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 92.08 && time <= 95.08)
    {
        if (time >= 92.08 && time <= 93.09)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 93.09 && time <= 94.02)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 94.02 && time <= 95.04)
            sub = "<span>​Ooh This my s..., this my s...</span>";

    }
    else if (time >= 96.20 && time <= 97.17)
        sub = "<span>​Ooh </span> This my s..., this my s...";
    if (time >= 97.17 && time <= 98.09)
        sub = "<span>​Ooh This my s...,</span> this my s...";
    if (time >= 98.09 && time <= 99.16)
        sub = "<span>​Ooh This my s..., this my s...</span>";
    else if (time >= 99.16)
    {
        sub = "";
    }
    document.getElementById("sub_current").innerHTML = sub;
}


function executePop() {


    if (time > 0 && time <= 4.02)
    {
        if (time > 0.14 && time <= 1.0)
            sub = "​<span>Ooh</span> All the girls stamp, your feet like this";
        if (time > 2.01 && time <= 3.0)
            sub = "​<span>Ooh All the girls</span> stamp,​ your feet like this";
        if (time > 3.0 && time <= 3.15)
            sub = "​<span>Ooh All the girls stamp,​ your feet</span> like this";
        if (time > 3.15 && time <= 4.02)
            sub = "​<span>Ooh All the girls stamp,​ your feet like this</span>";
    }
    else if (time >= 4.07 && time <= 8.13)
    {
        if (time >= 4.07 && time <= 5.04)
            sub = "​​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time > 5.04 && time <= 5.21)
            sub = "​<span>Few times​ I've been around</span> that track, So it's not just gonna happen like that";
        if (time > 5.21 && time <= 6.1)
            sub = "​<span>Few times I've been around that track</span>, So it's not just gonna happen like that";
        if (time > 6.1 && time <= 7.04)
            sub = "​<span>Few times I've been around that track, So it's not just</span> gonna happen like that";
        if (time > 7.04 && time <= 8.02)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen</span> like that";
        if (time > 8.02 && time <= 8.13)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 8.16 && time <= 13)
    {
        if (time >= 8.17 && time <= 9.0)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 9.07 && time <= 10.5)
            sub = "​<span>Cause I ain't no hollaback</span>​ girl I ain't no hollaback girl";
        if (time >= 10.5 && time <= 10.22)
            sub = "​<span>Cause I ain't no hollaback girl</span>​ I ain't no hollaback girl";
        if (time >= 10.22 && time <= 11.12)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no</span>​ hollaback girl";
        if (time >= 11.13 && time <= 12.09)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no​ hollaback</span> girl";
        if (time >= 12.09 && time <= 13)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no​ hollaback girl</span>";
    }
    else if (time >= 12.26 && time <= 17.09)
    {
        if (time >= 12.26 && time <= 14)
            sub = "​​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 13.2 && time <= 14.14)
            sub = "​<span>Few times​ I've been around</span> that track, So it's not just gonna happen like that";
        if (time >= 14.14 && time <= 14.23)
            sub = "​<span>Few times I've been around that track,​</span> So it's not just gonna happen like that";
        if (time >= 15.2 && time <= 16.09)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
        if (time >= 16.09 && time <= 17.09)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span> ";

    }
    else if (time >= 17 && time <= 22.01)
    {
        if (time >= 16.27 && time <= 17.2)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 17.3 && time <= 18.22)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 18.22 && time <= 19.13)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 19.13 && time <= 21.01)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback</span> girl";
        if (time >= 21.01 && time <= 22.01)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 22.09 && time <= 25)
    {
        if (time >= 22.09 && time <= 23.15)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 23.15 && time <= 24.09)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 24.09 && time <= 25)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 25.2 && time <= 29.15)
    {
        if (time >= 25.2 && time <= 27.22)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 27.22 && time <= 28.1)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 28.1 && time <= 29.15)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 30.12 && time <= 35)
    {
        if (time >= 29.16 && time <= 31.1)
            sub = "​​<span>So that's right dude,</span> meet me at the bleachers No principals, no student-teachers";
        if (time >= 30.1 && time <= 32)
            sub = "​​<span>So that's right dude, meet me at the bleachers</span> No principals, no student-teachers";
        if (time >= 32 && time <= 33.16)
            sub = "​​<span>So that's right dude, meet me at the bleachers No principals,</span> no student-teachers";
        if (time >= 33.16 && time <= 35)
            sub = "​​<span>So that's right dude, meet me at the bleachers No principals, no student-teachers</span>";
    }
    else if (time >= 35 && time <= 39)
    {
        if (time >= 35 && time <= 35.20)
            sub = "<span>​​​Both of us wanna be</span> the winner, but there can only be one";
        if (time >= 36.09 && time <= 37)
            sub = "<span>​​​Both of us wanna be the winner,</span> but there can only be one";
        if (time >= 38 && time <= 39)
            sub = "​​​<span>Both of us wanna be the winner, but there can only be one</span>";
    }
    else if (time >= 39 && time <= 43.2)
    {
        if (time >= 39 && time <= 40)
            sub = "​<span>So I'm gonna fight,</span> gonna give it my all, Gonna make you fall, gonna sock it to ya";
        if (time >= 40 && time <= 41.06)
            sub = "​<span>So I'm gonna fight, gonna give it my all,</span> Gonna make you fall, gonna sock it to ya";
        if (time >= 41.06 && time <= 42.08)
            sub = "​<span>So I'm gonna fight, gonna give it my all, Gonna make you fall,</span> gonna sock it to ya";
        if (time >= 42.08 && time <= 43.2)
            sub = "​<span>So I'm gonna fight, gonna give it my all, Gonna make you fall, gonna sock it to ya</span>";
    }
    else if (time >= 43.24 && time <= 47.20)
    {
        if (time >= 43.24 && time <= 44.1)
            sub = "<span>​That's right,</span> I'm the last one standing, and another one bites the dust";
        if (time >= 44.1 && time <= 45.18)
            sub = "<span>​That's right, I'm the last one standing,</span> and another one bites the dust";
        if (time >= 45.18 && time <= 46.10)
            sub = "​<span>That's right, I'm the one standing, and another one</span> bites the dust";
        if (time >= 46.10 && time <= 47.20)
            sub = "​<span>That's right, I'm the one standing, and another one bites the dust</span>";
    }
    else if (time >= 48.08 && time <= 52.05)
    {
        if (time >= 48.06 && time <= 48.23)
            sub = "​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 48.23 && time <= 49.22)
            sub = "​<span>Few times I've been around that track,</span> So it's not just gonna happen like that";
        if (time >= 50.05 && time <= 51)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
        if (time >= 51 && time <= 52.04)
            sub = "​<span>Few times I've been around that track, So it's not just gonna happen like that</span>";

    }
    else if (time >= 52 && time <= 56.18)
    {
        if (time >= 52.11 && time <= 52.19)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 52.19 && time <= 53.16)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 53.16 && time <= 55.22)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 55.22 && time <= 56.18)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 57.15 && time <= 63.12)
    {
        if (time >= 57.15 && time <= 58.15)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 58.15 && time <= 59.09)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 62.01 && time <= 63.12)
            sub = "<span>​Ooh This my s..., this my s...</span>";
    }
    else if (time >= 63.23 && time <= 69.15)
    {
        if (time >= 63.23 && time <= 65.07)
            sub = "​<span>Let me hear you say,</span> this s... is bananas, B-A-N-A-N-A-S";
        if (time >= 66.07 && time <= 67.04)
            sub = "​<span>Let me hear you say, this s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 67.04 && time <= 69.15)
            sub = "​<span>Let me hear you say, this s... is bananas, B-A-N-A-N-A-S</span>";
    }
    else if (time >= 70.16 && time <= 71.23)
    {
        if (time >= 70.1 && time <= 70.15)
            sub = "<span>is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 70.15 && time <= 71.23)
            sub = "<span>is bananas, B-A-N-A-N-A-S</span>";

    }
    else if (time >= 73.11 && time <= 78.08)
    {
        if (time >= 74.03 && time <= 74.16)
            sub = "<span>Again This s...</span> is bananas, B-A-N-A-N-A-S";
        if (time >= 74.16 && time <= 76.16)
            sub = "<span>Again This s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 76.16 && time <= 78.08)
            sub = "<span>Again This s... is bananas, B-A-N-A-N-A-S</span>";
    }
    else if (time >= 78.08 && time <= 82.17)
    {
        if (time >= 78.11 && time <= 79.03)
            sub = "<span>This s...</span> is bananas, B-A-N-A-N-A-S";
        if (time >= 79.10 && time <= 80.15)
            sub = "<span>This s... is bananas,</span> B-A-N-A-N-A-S";
        if (time >= 80.19 && time <= 82.16)
            sub = "<span>This s... is bananas,B-A-N-A-N-A-S</span>";
    }
    else if (time >= 83 && time <= 87.03)
    {
        if (time >= 83 && time <= 83.22)
            sub = "​<span>Few times</span> I've been around that track, So it's not just gonna happen like that";
        if (time >= 83.22 && time <= 86.03)
            sub = "​<span>Few times I've been around that track,</span> So it's not just gonna happen like that";
        if (time >= 86.03 && time <= 87.02)
            sub = "​<span>Few times I've been around that track, So it's not just gonna</span> happen like that";
    }
    else if (time >= 87.3 && time <= 91.22)
    {
        if (time >= 87.3 && time <= 87.15)
            sub = "​<span>Cause I ain't</span> no hollaback girl I ain't no hollaback girl";
        if (time >= 87.15 && time <= 88.15)
            sub = "​<span>Cause I ain't no hollaback</span> girl I ain't no hollaback girl";
        if (time >= 88.15 && time <= 89.11)
            sub = "​<span>Cause I ain't no hollaback girl</span> I ain't no hollaback girl";
        if (time >= 89.11 && time <= 90.17)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback</span> girl";
        if (time >= 90.17 && time <= 90.22)
            sub = "​<span>Cause I ain't no hollaback girl I ain't no hollaback girl</span>";
    }
    else if (time >= 92.08 && time <= 95.08)
    {
        if (time >= 92.08 && time <= 93.09)
            sub = "<span>​Ooh </span> This my s..., this my s...";
        if (time >= 93.09 && time <= 94.02)
            sub = "<span>​Ooh This my s...,</span> this my s...";
        if (time >= 94.02 && time <= 95.04)
            sub = "<span>​Ooh This my s..., this my s...</span>";

    }
    else if (time >= 96.20 && time <= 97.17)
        sub = "<span>​Ooh </span> This my s..., this my s...";
    if (time >= 97.17 && time <= 98.09)
        sub = "<span>​Ooh This my s...,</span> this my s...";
    if (time >= 98.09 && time <= 99.16)
        sub = "<span>​Ooh This my s..., this my s...</span>";
    else if (time >= 99.16)
    {
        sub = "";
    }
    document.getElementById("sub_current").innerHTML = sub;
}