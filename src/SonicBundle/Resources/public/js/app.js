function restore(){
  Fr.voice.stop();
}

function isMobile() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
}
$(document).ready(function(){
	if(isMobile()) 
	{
		var record = false;
		$(window).scroll(function (event) {
			var scroll = $(window).scrollTop();
			if(scroll >= 0)
			{
				$(window).scrollTop(0);
				if(record == false)
				{
					elem = $('#record');
					Fr.voice.record($("#live").is(":checked"), function(){
						elem.addClass("disabled");
						$("#live").addClass("disabled");
						$("#play_btn").removeClass("disabled");
						$(".one").removeClass("disabled");

						/**
						* The Waveform canvas
						*/
						analyser = Fr.voice.context.createAnalyser();
						analyser.fftSize = 2048;
						analyser.minDecibels = -90;
						analyser.maxDecibels = -10;
						analyser.smoothingTimeConstant = 0.85;
						Fr.voice.input.connect(analyser);

						var bufferLength = analyser.frequencyBinCount;
						var dataArray = new Uint8Array(bufferLength);

						WIDTH = 500, HEIGHT = 200;
						canvasCtx = $("#level")[0].getContext("2d");
						canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

						function draw() {

						drawVisual = requestAnimationFrame(draw);
						document.getElementById("pichAudio").innerHTML = dataArray[0];
						analyser.getByteTimeDomainData(dataArray);
						canvasCtx.fillStyle = 'rgb(3, 3, 2)';
						canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
						canvasCtx.lineWidth = 2;
						canvasCtx.strokeStyle = 'rgb(239, 181, 35)';

						canvasCtx.beginPath();
						var sliceWidth = WIDTH * 1.0 / bufferLength;
						var x = 0;
						for(var i = 0; i < bufferLength; i++) {
						  var v = dataArray[i] / 128.0;
						  var y = v * HEIGHT/2;

						  if(i === 0) {
							canvasCtx.moveTo(x, y);
						  } else {
							canvasCtx.lineTo(x, y);
						  }

						  x += sliceWidth;
						}
						canvasCtx.lineTo(WIDTH, HEIGHT/2);
						canvasCtx.stroke();
						};
						draw();
					});
					record = true
				}
			}
		});
	}
	else
	{
		$(document).on("click", "#record:not(.disabled)", function(){
		elem = $(this);
		Fr.voice.record($("#live").is(":checked"), function(){
		  elem.addClass("disabled");
		  $("#live").addClass("disabled");
		  $("#play_btn").removeClass("disabled");
		  $(".one").removeClass("disabled");

		  /**
		   * The Waveform canvas
		   */
		  analyser = Fr.voice.context.createAnalyser();
		  analyser.fftSize = 2048;
		  analyser.minDecibels = -90;
		  analyser.maxDecibels = -10;
		  analyser.smoothingTimeConstant = 0.85;
		  Fr.voice.input.connect(analyser);
		  
		  var bufferLength = analyser.frequencyBinCount;
		  var dataArray = new Uint8Array(bufferLength);
		  
		  WIDTH = 500, HEIGHT = 200;
		  canvasCtx = $("#level")[0].getContext("2d");
		  canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);
		  
		  function draw() {
			
			drawVisual = requestAnimationFrame(draw);
			document.getElementById("pichAudio").innerHTML = dataArray[0];
			analyser.getByteTimeDomainData(dataArray);
			canvasCtx.fillStyle = 'rgb(3, 3, 2)';
			canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
			canvasCtx.lineWidth = 2;
			canvasCtx.strokeStyle = 'rgb(239, 181, 35)';
	  
			canvasCtx.beginPath();
			var sliceWidth = WIDTH * 1.0 / bufferLength;
			var x = 0;
			for(var i = 0; i < bufferLength; i++) {
			  var v = dataArray[i] / 128.0;
			  var y = v * HEIGHT/2;
	  
			  if(i === 0) {
				canvasCtx.moveTo(x, y);
			  } else {
				canvasCtx.lineTo(x, y);
			  }
	  
			  x += sliceWidth;
			}
			canvasCtx.lineTo(WIDTH, HEIGHT/2);
			canvasCtx.stroke();
		  };
		  draw();
		});
	  });
	}
  
  
  $(document).on("click", "#stop:not(.disabled)", function(){
    restore();
  });
  
  $(document).on("click", "#play:not(.disabled)", function(){
    Fr.voice.export(function(url){
      $("#audio").attr("src", url);
      $("#audio")[0].play();
    }, "URL");
    restore();
  });
  
  $(document).on("click", "#download:not(.disabled)", function(){
    Fr.voice.export(function(url){
      $("<a href='"+url+"' download='MyRecording.wav'></a>")[0].click();
    }, "URL");
    restore();
  });
  
  $(document).on("click", "#base64:not(.disabled)", function(){
    Fr.voice.export(function(url){
      console.log("Here is the base64 URL : " + url);
      alert("Check the web console for the URL");
      
      $("<a href='"+ url +"' target='_blank'></a>")[0].click();
    }, "base64");
    restore();
  });
  
  $(document).on("click", "#mp3:not(.disabled)", function(){
    alert("The conversion to MP3 will take some time (even 10 minutes), so please wait....");
    Fr.voice.export(function(url){
      console.log("Here is the MP3 URL : " + url);
      alert("Check the web console for the URL");
      
      $("<a href='"+ url +"' target='_blank'></a>")[0].click();
    }, "mp3");
    restore();
  });
  
  $(document).on("click", "#save:not(.disabled)", function(){
    Fr.voice.export(function(blob){
      var formData = new FormData();
      formData.append('file', blob);
  
      $.ajax({
                url: "/server/upload_record.php",
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function(url) {
          $("#audio").attr("src", url);
          $("#audio")[0].play();
          alert("Guardando grabacion");
        }
      });
    }, "blob");
    restore();
  });
});
