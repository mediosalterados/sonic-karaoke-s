/*
 Created on : Jul 11, 2015, 2:36:57 AM
 Author     : ZER0
 */
/* global retaste_url, FB, access_token */
var sliders = [];
var friendsIDarray = [];
var user_friend_list;
var challenge_url;
var post_url = "https://www.sonickaraoke.mx/homepage";
var post_image_url = "https://www.sonickaraoke.mx/bundles/sonic/images/share/facebook_post_share.jpg";
var friend_to_post;

$(window).on("scrollstart", function (event) {
    if ($("html").hasClass('is-mobile')) {
        toggleFullScreen();
    }
});


$(window).on("orientationchange", function (event) {
    if (event.orientation === 'portrait') {
        $('.orientation-wrap').removeClass('hidden');
    } else {
        $('.orientation-wrap').addClass('hidden');
    }
});

$(document).ready(function () {
    if ($("html").hasClass('is-mobile')) {
        $(window).orientationchange();
    }

    $('.safari-not-available').click(function () {
        window.location = "http://www.chevrolet.com.mx/sonic.html";
    });

    $.ajaxSetup({cache: true});
    $.getScript('//connect.facebook.net/es_MX/sdk.js', function () {
        window.fbAsyncInit = function () {
            FB.init({
                appId: '948687841944677',
                version: 'v2.7' // or v2.1, v2.2, v2.3, ...
            });
        }
    });
    if ($(".slider-content").length) {
        $(".slider-content").each(function () {
            sliders.push($(this).bxSlider({
                slideMargin: $(this).data('slide-margin') || 0,
                moveSlides: $(this).data('move-slides') || 1,
                mode: $(this).data('mode') || 'horizontal',
                slideWidth: $(this).data('slide-width') || 0,
                maxSlides: $(this).data('max-slides') || 1,
                minSlides: $(this).data('max-slides') || 1,
                auto: hasData($(this).data('auto')) ? $(this).data('auto') : true,
                pager: hasData($(this).data('pager')) ? $(this).data('pager') : false || true,
                controls: hasData($(this).data('controls')) ? $(this).data('controls') : false || true,
                speed: $(this).data('speed') || 800,
                responsive: hasData($(this).data('responsive')) ? $(this).data('responsive') : false || true,
                touchEnabled: hasData($(this).data('touch-enabled')) ? $(this).data('touch-enabled') : false || true
            }));
        });
    }

    $(".btn-continue").click(function () {
        sliders[0].goToNextSlide();
    });
    if ($(".fancybox").length) {
        $(".fancybox").fancybox();
    }


    $('a.do-share').click(function () {

        if (typeof (image_url_post) !== 'undefined') {
            $.ajax({
                dataType: 'json',
                url: image_url_post,
                success: function (response) {
                    if (response.status === 'ok') {
                        post_image_url = "https://sonickaraoke.mx/server/posting/" + response.img;
                        shareSite();
                    }
                }
            });
        } else {
            shareSite();
        }
    });
    $('a.do-share-home').click(function () {
        shareHome();
    });
    $('form.ajax-submit').submit(function () {

        if (!challenge_url) {
            var form = $(this);
            $.ajax({
                dataType: 'json',
                url: form.attr('action'),
                type: 'post',
                data: form.serialize(),
                success: function (response) {
                    if (response.status === 'ok') {
                        challenge_url = response.url_challenge;
                        meTaggableFriends();
                    }
                }
            });
        } else {
            meTaggableFriends();
        }

        return false;
    });

    $('.circle-content-genero').click(function () {
        $('.circle-content-genero').removeClass('active');
        $(this).find('input[type="radio"]').prop('checked', true);
        $(this).addClass('active');
        var btn = $(this);
        if ($(".device-position-wrap").hasClass('is-mobile')) {
            $(".device-position-wrap").removeClass('hidden');
            window.setTimeout(function () {
                btn.closest('.container').find('.a-submit').trigger('click');
            }, 5000);
        } else {
            btn.closest('.container').find('.a-submit').trigger('click');
        }
    });

    $('.a-submit').click(function () {
        var f = $(this).parents('form');
        f.submit();
    });

    $(document).on('click', ".btn-hide-wrap", function () {
        var target = $(this).data('target');
        $("." + target).addClass('hidden');
        return false;
    });

    $(".txt-fx-fall-ltr").letterfx({"fx": "fade fall fly-left", "backwards": false, "timing": 50, "fx_duration": "100ms", "letter_end": "stay", "element_end": "stay"});
    $(".txt-fx-fall-rtl").letterfx({"fx": "fade fall fly-right", "backwards": false, "timing": 50, "fx_duration": "100ms", "letter_end": "stay", "element_end": "stay"});
    $(".txt-fx-fall-ttb").letterfx({"fx": "fade fall fly-top", "backwards": false, "timing": 50, "fx_duration": "100ms", "letter_end": "stay", "element_end": "stay"});
    $(".txt-fx-wave").letterfx({"fx": "wave", "backwards": false, "timing": 50, "fx_duration": "50ms", "letter_end": "rewind", "element_end": "restore"});

});
function hasData(data) {
    return  typeof (data) !== undefined && data !== null ? true : false;
}


function fontSize(mViewport, dViewport) {
    mViewport = mViewport || 320;
    dViewport = dViewport || 2015;
    var containerWidth = $("html").width();
    var htmlFS;
    if ($("html").hasClass('is-mobile')) {
        htmlFS = containerWidth * 10 / mViewport;
    } else {
        htmlFS = containerWidth * 10 / dViewport;
    }
    $("html").css({"font-size": htmlFS});
}

function meTaggableFriends() {

    if (typeof (access_token) !== 'undefined') {
        doFriendSelector();
    } else {
        FB.login(function (response) {
            if (response.authResponse) {
                access_token = response.authResponse.accessToken;
                doFriendSelector();
            }
        }, {scope: 'email,publish_actions,user_photos,public_profile,user_friends,user_posts'});
    }





}

function doFriendSelector() {
    $('.overlay-loader-wrap').removeClass('hidden');
    FB.api("/me/taggable_friends?fields=id,name,picture.width(400).height(400)&limit=1000",
            function (response) {
                if (response && !response.error) {

                    var selected_list = $('<ul class="friends-list full-width"></ul>');

                    for (var i = 0; i < response.data.length; i++) {
                        var data = response.data;
                        friendsIDarray.push(data[i].id);
                        selected_list.append('<li class="friend-wrap"><a data-friend-id="' + data[i].id + '" data-friend-image="' + data[i].picture.data.url + '" data-friend-name="' + data[i].name + '" class="friend_selector" href="#"><span class="bordered-wrap profile-thumb"><span class="image-wrap circle-image-wrap"><img src="' + data[i].picture.data.url + '" class="responsive-image" /></span></span><span class="friend-name-wrap">' + data[i]['name'] + '</span></a></li>');
                    }

                    $('.friend-list-wrap').append(selected_list);

                    $(".overlay-main-wrap").removeClass('hidden');
                    $('.overlay-loader-wrap').addClass('hidden');
                    $('.friend-wrap a').click(friendSelected);
                }
            }
    , {access_token: access_token});
}

function friendSelected() {
    var a = $(this);

    Cookies.set('friend_data_image', a.data('friend-image'), {expires: 7, path: '/'});
    Cookies.set('friend_data_name', a.data('friend-name'), {expires: 7, path: '/'});

    var li = a.parent('li');

    li.addClass('selected-friend');


    $('li.friend-wrap').not(li).fadeTo('fast', '.30');
    li.fadeTo('fast','1');

    $('a.friend-selector').removeClass('hidden');
    
    friend_to_post = a.data('friend-id');
}

function postToFriend() {
    $(".overlay-main-wrap").addClass('hidden');
    $('.overlay-loader-wrap').removeClass('hidden');

    var message = "Reta a otro interprete y descubre quien es mejor.";

    FB.api('/me/feed?caption=' + message + '&picture=' + post_image_url + '&name=' + '¿Buscas competencia?' + '&link=' + challenge_url + '&tags[]=' + friend_to_post, 'post', {message: "body"}, function (response) {
        if (!response || response.error) {
        } else {
            window.location = retaste_url;
        }
    }, {access_token: access_token});

}


function shareSite() {
    FB.ui({
        method: 'feed',
        link: post_url,
        picture: post_image_url,
        name: "Chevrolet Sonic Karaoke",
        caption: "Llego la hora de que el mundo conozca tu talento",
        description: "Comparte el karaoke con tus amigos, ellos seguro también quieren cantar."
    }, function (response) {
    });
}
function shareHome() {
    FB.ui({
        method: 'feed',
        link: post_url,
        picture: post_image_url,
        name: "Chevrolet Sonic Karaoke",
        caption: "Este es el sitio donde la diversión se canta",
        description: "¿Qué tan bueno eres en el Karaoke? Saca la diversión que llevas dentro y demuestra el poder de tu voz."
    }, function (response) {
    });
}

function sharePost(object) {
    if (typeof (access_token) !== 'undefined') {
        FB.ui(object, postResponse, {access_token: access_token});
    } else {
        FB.ui(object, postResponse);
    }
}

function postResponse(response) {
    if (response.post_id) {
        window.location = retaste_url;
    } else {
        alert('error al compartir');
    }
}

function toggleFullScreen() {
    var doc = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    }
}