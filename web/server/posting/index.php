<?php

// link to the font file no the server
$fontname = 'font/LouisItalic.ttf';
// controls the spacing between text
$i = 30;
//JPG image quality 0-100
$quality = 100;

function create_image($user) {

    global $fontname;
    global $quality;
    $file = "covers/" . md5(time() . $_REQUEST['score']) . ".jpg";

    // if the file already exists dont create it again just serve up the original	
    //if (!file_exists($file)) {	
    // define the base image that we lay our text on
    $im = imagecreatefromjpeg("pass.jpg");

    // setup the text colours
    $color['white'] = imagecolorallocate($im, 255, 255, 255);
    $color['yellow'] = imagecolorallocate($im, 230, 174, 33);

    // this defines the starting height for the text block
    $y = imagesy($im) - $height - 410;

    // loop through the array and write the text	
    foreach ($user as $pos => $value) {
        
        // center the text in our image - returns the x value
        $x = 640 + center_text($value['name'], $value['font-size']);
        imagettftext($im, $value['font-size'], 0, $x, $y + $i, $color[$value['color']], $fontname, $value['name']);
        // add 32px to the line height for the next text block

        if ($pos < 1) {
            $i = $i + 70;
        } else {
            $i = $i + 80;
        }
    }
    // create the image
    imagejpeg($im, $file, $quality);

    //}

    return $file;
}

function center_text($string, $font_size) {

    global $fontname;

    $image_width = 640;
    $dimensions = imagettfbbox($font_size, 0, $fontname, $string);

    return ceil(($image_width - $dimensions[4]) / 2);
}

$nick_name = $_REQUEST['username'];

$user = array(
    array(
        'name' => $nick_name,
        'font-size' => '50',
        'color' => 'yellow'),
    array(
        'name' => 'Score:',
        'font-size' => '50',
        'color' => 'yellow'),
    array(
        'name' => $_REQUEST['score'],
        'font-size' => '60',
        'color' => 'white'),
);



// run the script to create the image
$filename = create_image($user);

header('Content-Type: application/json');
echo json_encode(array('status' => 'ok', 'img' => $filename));
?>

